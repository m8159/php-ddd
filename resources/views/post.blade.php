<?php
/**
 * @var \App\Models\Post $post
 */
?>

<x-layout>
    <article>
        <h1>{{$post->title}}</h1>
        <div>{!! $post->body !!}</div>
    </article>

    <a href="/">
        <x-button>
            Go Back
        </x-button>
    </a>
</x-layout>