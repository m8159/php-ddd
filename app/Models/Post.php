<?php

namespace App\Models;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Spatie\YamlFrontMatter\Document;
use Spatie\YamlFrontMatter\YamlFrontMatter;

class Post
{
    public function __construct(
        public readonly string $title,
        public readonly string $excerpt,
        public readonly string $date,
        public readonly string $body,
        public readonly string $slug
    ) {}

    public static function findOrFail(string $slug): Post
    {
        if (!$post = static::find($slug)) {
            throw new ModelNotFoundException("Post '$slug' was not found");
        }

        return $post;
    }

    public static function find(string $slug): ?Post
    {
        $posts = static::all();
        return $posts->firstWhere('slug', $slug);
    }

    public static function all(): Collection
    {
        return cache()->remember('posts.all', now()->addMinutes(5), function () {
            return collect(File::files(resource_path("/posts")))
                ->map(fn(string $file) => YamlFrontMatter::parseFile($file))
                ->map(fn(Document $document) => new Post(
                    title: $document->matter('title'),
                    excerpt: $document->matter('excerpt'),
                    date: $document->matter('date'),
                    body: $document->body(),
                    slug: $document->matter('slug')
                ))->sortByDesc('date');
        });
    }
}